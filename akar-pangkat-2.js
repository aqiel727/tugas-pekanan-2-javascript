const prompt = require('prompt-sync')({sigint: true});

function akarPangkatDua (x) {
    if (x < 0 ) {
        console.log("Tidak bisa input bilangan negatif");
    } else if (x % 2 !== 0) {
        console.log("Tidak bisa input bilangan ganjil");
    } else {
        return Math.sqrt(x)
    }
}

var angka = parseInt(prompt("masukan angka genap: "));
var hasil = parseInt(akarPangkatDua(angka));
console.log(`Akar pangkat dua dari ${angka} adalah: ${hasil}`);
